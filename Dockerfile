FROM julia:1.8.5-bullseye
WORKDIR /app
COPY . .
ENV  JULIA_DEPOT_PATH=/app/.julia
RUN chmod +x docker_build.sh
RUN /app/docker_build.sh

ENTRYPOINT ["julia", "--project=.", "gen_db_tmp.jl"]

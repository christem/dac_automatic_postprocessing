#!/bin/sh

julia --project=. -e 'using Pkg; Pkg.Registry.add(RegistrySpec(name="General", url="https://github.com/JuliaRegistries/General.git"))'
julia --project=. -e 'using Pkg; Pkg.Registry.add(RegistrySpec(url = "https://gitlab.cern.ch/christem/julia_local_registry"))'
julia --project=. -e 'using Pkg; Pkg.instantiate()'
julia --project=. -e 'using Pkg; Pkg.build("qd_analysis")'
julia --project=. -e 'using Pkg; Pkg.build("qd_loader")'

chgrp -R 0 /app && chmod -R g+rwX /app
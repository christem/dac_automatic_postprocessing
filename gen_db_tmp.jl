using qd_analysis
using qd_loader 
using LibPQ
using Tables
using JSON3
using FileIO
using TimeZones
using Dates


# connect = "host=dbod-qd-pm-acquisition.cern.ch port=6600 dbname=postmortems user= password="

# connect = create_connection_string(ENV["DB_HOST"], ENV["DB_PORT"], ENV["DB_NAME"], ENV["DB_USER"], ENV["DB_PASSWORD"])
# conn = LibPQ.Connection(connect)

# result = execute(conn, "SELECT id,event_time,event_metadata->>'name' FROM postmortem_events WHERE event_metadata->>'name' LIKE \$1", ["%dac_test_10%"])

# data = columntable(result)

# close(conn) 

# ids = convert(Vector{Int32},data[1])
# # ids = collect(15942:15956)

# println(ids)
# select = "SELECT * FROM postmortem_events WHERE id = "
# #acquisition, resp, channels = load_dac_acq_event(connect, select, [ids[1]])
# # responses = extract_frequency_response.(acquisition)
# # response_norm = norm_frq_response.(responses, resp)
# # chan_fft = single_sided_fft.(acquisition, truncate=true)
# channels = ["channel_1";"channel_2";"channel_3"]
# for idx in ids
#     println(idx)

#     acquisition, calibrations = load_dac_acq_event(ENV["DB_HOST"], ENV["DB_PORT"], ENV["DB_NAME"], ENV["DB_USER"], ENV["DB_PASSWORD"], select * string(idx))
#     println(size(acquisition[1]))
#     chan_fft = single_sided_fft.(acquisition[1], truncate=true)
#     responses = extract_frequency_response.(acquisition[1])
#     conn = LibPQ.Connection(connect)
    

#     for (channel_id, resp, fft) in zip(channels, responses, chan_fft)
#         result = execute(conn, "INSERT INTO phd_spectrum_post_mortem_buffers (event_id, channel_id, fft_magnitude, fft_frequency_resolution, response_magnitude , response_phase, response_frequency) VALUES (\$1, \$2, \$3, \$4, \$5, \$6, \$7) ;", [idx, channel_id, abs.(fft.response), fft.frequency[2], abs.(resp.response), rad2deg.(angle.(resp.response)), resp.frequency])
#     end

#     close(conn)
# end
connect = create_connection_string(ENV["DB_HOST"], ENV["DB_PORT"], ENV["DB_NAME"], ENV["DB_USER"], ENV["DB_PASSWORD"])
query = """
WITH deleted AS (
    DELETE FROM work_queue
    WHERE work_id = (
        SELECT work_id FROM work_queue
        ORDER BY work_id
        LIMIT 1
    )
    RETURNING id
)
SELECT * FROM deleted;
"""
select = "SELECT * FROM postmortem_events WHERE id = "
channels = ["channel_1";"channel_2";"channel_3"]
while true
    conn = LibPQ.Connection(connect)

    result = execute(conn, query)

    data = columntable(result)

    close(conn) 

    ids = convert(Vector{Int32},data[1])

    if length(ids) == 1
        idx = ids[1]
        println(idx)
        acquisition, calibrations = load_dac_acq_event(ENV["DB_HOST"], ENV["DB_PORT"], ENV["DB_NAME"], ENV["DB_USER"], ENV["DB_PASSWORD"], select * string(idx))
        chan_fft = single_sided_fft.(acquisition[1], truncate=true)
        responses = extract_frequency_response.(acquisition[1], A_qr=false)
        conn = LibPQ.Connection(connect)
        for (channel_id, resp, fft) in zip(channels, responses, chan_fft)
            result = execute(conn, "INSERT INTO phd_spectrum_post_mortem_buffers (event_id, channel_id, fft_magnitude, fft_frequency_resolution, response_magnitude , response_phase, response_frequency) VALUES (\$1, \$2, \$3, \$4, \$5, \$6, \$7) ;", [idx, channel_id, abs.(fft.response), fft.frequency[2], abs.(resp.response), rad2deg.(angle.(resp.response)), resp.frequency])
        end
        close(conn)
    end


    sleep(1)
end